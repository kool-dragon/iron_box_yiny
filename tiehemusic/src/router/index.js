import Vue from 'vue'
import Router from 'vue-router'
import Layout from "../containers/Layout.vue"
import Home from '../containers/Home'
import Mine from '../containers/Mine'
import MusicList from '../containers/MusicList'
import Search from '../containers/Search'
import Singer from '../containers/Singer'
import Player from "../containers/Player"


Vue.use(Router)

export default new Router({
  linkActiveClass:"active",
  routes: [
    {
      path: '/',
      name: 'Layout',
      component: Layout,
      children:[
        {
          path:"/",
          component:Home
        },
        {
          path:"/mine",
          component:Mine
        },
        {
          path:"/musiclist",
          component:MusicList
        },
        {
          path:"/search",
          component:Search
        },
        {
          path:"/singer",
          component:Singer
        },
      ]
    },
    {
      path: '/player/:id',
      name: "Player",
      component: Player,
    }
  ]
})

